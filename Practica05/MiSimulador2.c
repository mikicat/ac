#include "CacheSim.h"
#include <stdio.h>

/* Posa aqui les teves estructures de dades globals
 * per mantenir la informacio necesaria de la cache
 * */
 struct data{
   int tag;
   unsigned int lru;
 };

 struct data mem[128];




/* La rutina init_cache es cridada pel programa principal per
 * inicialitzar la cache.
 * La cache es inicialitzada al comen�ar cada un dels tests.
 * */
void init_cache ()
{
    totaltime=0.0;
	/* Escriu aqui el teu codi */
  for (int i = 0; i < 128; i++) {
    mem[i].tag = -1;
    mem[i].lru = 1;
  }
}

/* La rutina reference es cridada per cada referencia a simular */
void reference (unsigned int address)
{
	unsigned int byte;
	unsigned int bloque_m;
	unsigned int conj_mc;
	unsigned int via_mc;
	unsigned int tag;
	unsigned int miss;	   // boolea que ens indica si es miss
	unsigned int replacement;  // boolea que indica si es reempla�a una linia valida
	unsigned int tag_out;	   // TAG de la linia reempla�ada
	float t1,t2;		// Variables per mesurar el temps (NO modificar)

	t1=GetTime();
	/* Escriu aqui el teu codi */
  byte = address & 0x1F;
  bloque_m = address >> 5;
  conj_mc = bloque_m & 0x3F;
  tag = address >> 11;
  miss = 0;
  replacement = 0;

  if (mem[conj_mc].tag == -1) {
    miss = 1;
    mem[conj_mc].tag = tag;
    mem[conj_mc].lru = 0;
    mem[conj_mc+64].lru = 1;
    via_mc = 0;
  }
  else if (mem[conj_mc].tag != tag && mem[conj_mc+64].tag != tag) {
    miss = 1;
    if (mem[conj_mc+64].tag == -1) {
      mem[conj_mc+64].tag = tag;
      mem[conj_mc+64].lru = 0;
      mem[conj_mc].lru = 1;
      via_mc = 1;
    }
    else if (mem[conj_mc].lru == 1) {
      tag_out = mem[conj_mc].tag;
      replacement = 1;
      mem[conj_mc].tag = tag;
      mem[conj_mc].lru = 0;
      mem[conj_mc+64].lru = 1;
      via_mc = 0;
    }
    else {
      tag_out = mem[conj_mc+64].tag;
      replacement = 1;
      mem[conj_mc+64].tag = tag;
      mem[conj_mc+64].lru = 0;
      mem[conj_mc].lru = 1;
      via_mc = 1;
    }
  }
  else {
    if (mem[conj_mc].tag == tag) {
      mem[conj_mc].lru = 0;
      mem[conj_mc+64].lru = 1;
      via_mc = 0;
    }
    else {
      mem[conj_mc].lru = 1;
      mem[conj_mc+64].lru = 0;
      via_mc = 1;
    }
  }


	/* La funcio test_and_print escriu el resultat de la teva simulacio
	 * per pantalla (si s'escau) i comproba si hi ha algun error
	 * per la referencia actual. Tamb� mesurem el temps d'execuci�
	 * */
	t2=GetTime();
	totaltime+=t2-t1;
	test_and_print2 (address, byte, bloque_m, conj_mc, via_mc, tag,
			miss, replacement, tag_out);
}

/* La rutina final es cridada al final de la simulacio */
void final ()
{
 	/* Escriu aqui el teu codi */
}
