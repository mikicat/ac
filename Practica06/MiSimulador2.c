#include "CacheSim.h"

/* Posa aqui les teves estructures de dades globals
 * per mantenir la informacio necesaria de la cache
 * */
struct data {
	int tag;
	int dirty;
};
struct data mem[128];


/* La rutina init_cache es cridada pel programa principal per
 * inicialitzar la cache.
 * La cache es inicialitzada al comen�ar cada un dels tests.
 * */
void init_cache ()
{
	/* Escriu aqui el teu codi */
	for (int i = 0; i < 128; ++i) {
		mem[i].tag = -1;
		mem[i].dirty = 0;
	}
}

/* La rutina reference es cridada per cada referencia a simular */
void reference (unsigned int address, unsigned int LE)
{
	unsigned int byte; // 5 bits LSB
	unsigned int bloque_m; // 27 bits MSB
	unsigned int linea_mc; // 7 bits
	unsigned int tag; // 20 bits MSB
	unsigned int miss;
	unsigned int lec_mp; // bool
	unsigned int mida_lec_mp; // 32 bytes
	unsigned int esc_mp; // bool
	unsigned int mida_esc_mp; //
	unsigned int replacement;
	unsigned int tag_out;

	/* Escriu aqui el teu codi */
	byte = address & 0x1F;
	bloque_m = address >> 5;
	linea_mc = bloque_m & 0x7F;
	tag = address >> 12;
	miss = 0;
	replacement = 0;
	lec_mp = 0;
	esc_mp = 0;

	if (mem[linea_mc].tag == -1) {
		lec_mp = 1;
		mida_lec_mp = 32;
		mem[linea_mc].tag = tag;
		miss = 1;
	}
	else if (mem[linea_mc].tag != tag) {
		lec_mp = 1;
		mida_lec_mp = 32;
		tag_out = mem[linea_mc].tag;
		mem[linea_mc].tag = tag;
		if (mem[linea_mc].dirty == 1) {
			esc_mp = 1;
			mida_esc_mp = 32;
			mem[linea_mc].dirty = 0;
		}
		miss = 1;
		replacement = 1;

	}

	if (LE == escriptura) {
		mem[linea_mc].dirty = 1;
	}


	/* La funcio test_and_print escriu el resultat de la teva simulacio
	 * per pantalla (si s'escau) i comproba si hi ha algun error
	 * per la referencia actual
	 * */
	test_and_print (address, LE, byte, bloque_m, linea_mc, tag,
			miss, lec_mp, mida_lec_mp, esc_mp, mida_esc_mp,
			replacement, tag_out);
}

/* La rutina final es cridada al final de la simulacio */
void final ()
{
 	/* Escriu aqui el teu codi */


}
