.text
	.align 4
	.globl procesar
	.type	procesar, @function
procesar:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	pushl	%ebx
	pushl	%esi
	pushl	%edi

# Aqui has de introducir el codigo

        movl 20(%ebp), %ecx # n
        imul %ecx, %ecx # n²
        movl 8(%ebp), %eax # @mata
        movl 12(%ebp), %edx # @matb
        movl 16(%ebp), %esi # @matc
        addl %eax, %ecx # @mata[n²]
for:
        cmpl %ecx, %eax
        jge end # @mata[i] >= @mata[n²]
        movdqa %xmm0, (%eax) # algun comentari guapo
        movdqa %xmm1, (%edx)
        pcmpgtb %xmm0, %xmm1
        movdqa (%esi), %xmm0

        addl $16, %eax
        addl $16, %edx
        addl $16, %esi
        jmp for
end:

# El final de la rutina ya esta programado

	emms	# Instruccion necesaria si os equivocais y usais MMX
	popl	%edi
	popl	%esi
	popl	%ebx
	movl %ebp,%esp
	popl %ebp
	ret
