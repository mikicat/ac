.text
	.align 4
	.globl procesar
	.type	procesar, @function
procesar:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	pushl	%ebx
	pushl	%esi
	pushl	%edi

# Aqui has de introducir el codigo
        movl 20(%ebp), %ecx # n
        imul %ecx, %ecx # n²
        movl 8(%ebp), %eax # @mata
        movl 12(%ebp), %edx # @matb
        movl 16(%ebp), %esi # @matc
        addl %eax, %ecx # @mata[n²]
for:
        cmpl %ecx, %eax
        jge end # @mata[i] >= @mata[n²]
        movb (%eax), %bl
        subb (%edx), %bl
        cmpb $0, %bl
        jle else
        movb $255, (%esi)
        jmp endif
else:
        movb $0, (%esi)
endif:
        incl %eax
        incl %edx
        incl %esi
        jmp for
end:


# El final de la rutina ya esta programado

	popl	%edi
	popl	%esi
	popl	%ebx
	movl %ebp,%esp
	popl %ebp
	ret
