 .text
	.align 4
	.globl BuscarElemento
	.type BuscarElemento,@function
BuscarElemento:
        pushl %ebp
        movl %esp, %ebp
        pushl %esi
        movl 16(%ebp), %edx # mid
        movl (%edx), %eax # *mid
        imul $12, %eax, %ecx # *mid * 12
        movl 32(%ebp), %esi # @v
        movl 4(%esi, %ecx), %ecx # v[*mid].k
        cmpl 24(%ebp), %ecx
        je fi
else:
        movl 12(%ebp), %esi # high
        movl (%esi), %ecx # *high
        cmpl %ecx, %eax
        jge segon_else
        movl %ecx, (%edx) # *mid = *high
        movl 8(%ebp), %ecx # low
        incl (%ecx) # (*low)++
        jmp fi_segon
segon_else:
        movl 8(%ebp), %ecx # low
        movl (%ecx), %ecx # *low
        movl %ecx, (%edx) # *mid = *low
        decl (%esi) # (*high)--
fi_segon:
        movl $-1, %eax                           
fi:
        popl %esi
        movl %ebp, %esp
        popl %ebp
        ret
