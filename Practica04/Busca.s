.text
 .align 4
 .globl Buscar
 .type Buscar,@function
Buscar:
       pushl %ebp
       movl %esp, %ebp
       subl $16, %esp
       movl $-1, %eax # trobat = -1
       movl $0, -16(%ebp) # low = 0
       movl $0, -8(%ebp) # mid = low = 0
       movl 24(%ebp), %ecx # N
       decl %ecx # high
       movl %ecx, -12(%ebp) # high = N-1
while:  cmpl %ecx, -16(%ebp)
       jg endwhile
       pushl 8(%ebp) # v
       pushl 20(%ebp) # X (final)
       pushl 16(%ebp) # X (mid)
       pushl 12(%ebp) # X (inici)
       leal -8(%ebp), %edx # &mid
       pushl %edx
       leal -12(%ebp), %edx # &high
       pushl %edx
       leal -16(%ebp), %edx # &low
       pushl %edx
       call BuscarElemento
       addl $28, %esp
       cmpl $0, %eax # trobat >= 0
       jge endwhile
       movl -12(%ebp), %ecx
       jmp while
endwhile:
       movl %ebp, %esp
       popl %ebp
       ret
