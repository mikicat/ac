.text
	.align 4
	.globl OperaMat
	.type	OperaMat, @function
OperaMat:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	pushl	%ebx
	pushl	%esi
	pushl	%edi
# Aqui has de introducir el codigo
	movl $0, -4(%ebp) # res = 0
	movl $0, %ebx # i
primer_for:
	cmpl $3, %ebx
	jge endprimer
	movl $0, %ecx # j
segon_for:
	cmpl $3, %ecx
	jge endsegon
	# @Matriz + 12*i + 4*i = @M + 16*i
	movl 8(%ebp), %edx # @M
	movl %ecx, %eax # copia j
	imul $16, %ebx, %esi
	addl (%edx, %esi), %eax # M[i][i]+j
	subl %eax, -4(%ebp) # res -= M[i][i]+j
	incl %ecx
	jmp segon_for
endsegon:
	addl 12(%ebp), %ebx # i += salto
	jmp primer_for
endprimer:
# El final de la rutina ya esta programado
	movl	-4(%ebp), %eax
	popl	%edi
	popl	%esi
	popl	%ebx
	movl %ebp,%esp
	popl %ebp
	ret
